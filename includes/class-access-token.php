<?php
defined( 'ABSPATH' ) or exit;
use GuzzleHttp;
use GuzzleHttp\Psr7\MultipartStream;
define("SUMSUB_SECRET_KEY", '1x5ePxK6xeLOpNUX2S7uBrgH4kWPmgg');//get_option('wss_setting_secret','')); // Example: Hej2ch71kG2kTd1iIUDZFNsO5C1lh5Gq
define("SUMSUB_APP_TOKEN",'sbx:FjvWdpFeRnnDzNXqm0Esg2Z.JyII57g8GIFSRkkTIhQF19CMwKP4Q96q');// get_option('wss_setting_token','')); // Example: tst:uY0CgwELmgUAEyl4hNWxLngb.0WSeQeiYny4WEqmAALEAiK2qTC96fBad
define("SUMSUB_TEST_BASE_URL",'https://api.sumsub.com');// get_option('wss_setting_token','https://api.sumsub.com'));
//print_r(SUMSUB_APP_TOKEN);
if ( ! class_exists( 'WSS_Access_Token' ) ) {
    class WSS_Access_Token{
        public static $token;
        public static $secret;
        public static $apiUrl;
        public function __construct($apiToken,$apiSecret,$apiUrl)
        {
            $this->token=$apiToken;
            $this->secret=$apiSecret;
            $this->apiUrl=$apiUrl;
        }
            public function createApplicant($externalUserId, $levelName)
            // https://developers.sumsub.com/api-reference/#creating-an-applicant
        {
            $requestBody = [
                'externalUserId' => $externalUserId
                ];

            $url = '/resources/applicants?levelName=' . $levelName;
            $request = new GuzzleHttp\Psr7\Request('POST', $this->apiUrl . $url);
            $request = $request->withHeader('Content-Type', 'application/json');
            $request = $request->withBody(GuzzleHttp\Psr7\stream_for(json_encode($requestBody)));

            $responseBody = $this->sendHttpRequest($request, $url)->getBody();
            return json_decode($responseBody)->{'id'};
        }
        public function sendHttpRequest($request, $url)
        {
            $client = new GuzzleHttp\Client();
            $ts = time();
    
            $request = $request->withHeader('X-App-Token', $this->token);
            $request = $request->withHeader('X-App-Access-Sig', $this->createSignature($ts, $request->getMethod(), $url, $request->getBody()));
            $request = $request->withHeader('X-App-Access-Ts', $ts);
            
            // Reset stream offset to read body in `send` method from the start
            $request->getBody()->rewind();
    
            try {
                $response = $client->send($request);
                if ($response->getStatusCode() != 200 && $response->getStatusCode() != 201) {
                    // https://developers.sumsub.com/api-reference/#errors
                    // If an unsuccessful answer is received, please log the value of the "correlationId" parameter.
                    // Then perhaps you should throw the exception. (depends on the logic of your code)
                }
            } catch (GuzzleHttp\Exception\GuzzleException $e) {
                error_log($e);
            }
    
            return $response;
        }
                private function createSignature($ts, $httpMethod, $url, $httpBody)
            {
                return hash_hmac('sha256', $ts . strtoupper($httpMethod) . $url . $httpBody, $this->secret);
            }
            public function addDocument($applicantId)
            // https://developers.sumsub.com/api-reference/#adding-an-id-document
        {
            $metadata = ['idDocType' => 'PASSPORT', 'country' => 'GBR'];
            $file = WSS_PLUGIN_DIR. '/sumsub-logo.png';
    
            $multipart = new MultipartStream([
                [
                    "name" => "metadata",
                    "contents" => json_encode($metadata)
                ],
                [
                    'name' => 'content',
                    'contents' => fopen($file, 'r')
                ],
            ]);
    
            $url = "/resources/applicants/" . $applicantId . "/info/idDoc";
            $request = new GuzzleHttp\Psr7\Request('POST', $this->apiUrl . $url);
            $request = $request->withBody($multipart);
    
            return $this->sendHttpRequest($request, $url)->getHeader("X-Image-Id")[0];
        }
        public function getApplicantStatus($applicantId)
        // https://developers.sumsub.com/api-reference/#getting-applicant-status-api
    {
        $url = "/resources/applicants/" . $applicantId . "/requiredIdDocsStatus";
        $request = new GuzzleHttp\Psr7\Request('GET', $this->apiUrl . $url);

        return $responseBody = $this->sendHttpRequest($request, $url)->getBody();
        return json_decode($responseBody);
    }

    public function getAccessToken($externalUserId, $levelName)
        // https://developers.sumsub.com/api-reference/#access-tokens-for-sdks
    {
        $url = "/resources/accessTokens?userId=" . $externalUserId . "&levelName=" . $levelName;
        $request = new GuzzleHttp\Psr7\Request('POST', $this->apiUrl . $url);

        return $this->sendHttpRequest($request, $url)->getBody();
    }

    }
}