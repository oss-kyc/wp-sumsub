<?php
defined( 'ABSPATH' ) or exit;
if ( ! class_exists( 'WSS_FrontEnd' ) ) {
    class WSS_FrontEnd{
        public function __construct() {
            add_shortcode('wp-sumsub',[&$this,'main_shortcode_callback']);
            add_action('wp_ajax_get_sumsub_access_token',[&$this,'ajax_get_sumsub_access_token']);
            add_action('wp_ajax_nopriv_get_sumsub_access_token',[&$this,'ajax_get_sumsub_access_token']);
        }
        public function main_shortcode_callback(){
            wp_enqueue_script('wp_sumsub_main');
            wp_enqueue_script('wp_sumsub_callback');
            $externalUserId = uniqid();
            $levelName = get_option('wss_setting_level','basic-kyc-level');
            $accessToken=WP_Sumsub::getAccessToken($externalUserId,$levelName);
            $testMode=get_option('wss_setting_test','0');
          //  print_r($testMode);
            ob_start();
            ?>
            <script type="text/javascript">
                let sumSubaccessToken='<?=$accessToken->token?>';
                let sumSubuserId='<?=$accessToken->userId?>';
                let sumSubTestMode=<?=!empty($testMode)?$testMode:0?>;
            </script>
            <div id="sumsub-websdk-container"></div>
            <?php
            return ob_get_clean();
        }
        public function ajax_get_sumsub_access_token(){
            check_ajax_referer( 'sumsub-get-access-token', 'security' );
           
            $externalUserId = $_POST['userId'];
         
            $levelName =get_option('wss_setting_level','basic-kyc-level');
            $accessToken=WP_Sumsub::getAccessToken($externalUserId,$levelName);
            if(!empty($accessToken)){
                wp_send_json_success(['accessToken'=>$accessToken->token]);
            }
            wp_send_json_error();
        }
    }
    new WSS_FrontEnd();
}