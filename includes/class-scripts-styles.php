<?php
defined( 'ABSPATH' ) or exit;
if (!class_exists('WSS_Scripts_Styles')) {
    class WSS_Scripts_Styles{
        public function __construct(){
         add_action('wp_enqueue_scripts', [&$this, 'enqueue_styles']);
        }
        public function enqueue_styles(){
            wp_register_script('wp_sumsub_main','https://static.sumsub.com/idensic/static/sns-websdk-builder.js',[],null,false);
            wp_register_script('wp_sumsub_callback',WSS_PLUGIN_ABSOLUTE_PATH.'assets/js/sumsub.frontend.js',['jquery'],time(),true);
            wp_localize_script('wp_sumsub_callback','wp_sumsub_vars',[
                'ajax_url'=>admin_url('admin-ajax.php'),
                'nonces'    =>[
                    'get_access_token'=>wp_create_nonce( 'sumsub-get-access-token' )
                ]
           
            ]);

        }
    }
    new WSS_Scripts_Styles();

}