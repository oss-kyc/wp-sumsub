<?php
/*
Plugin Name: wp-sumsub
Description: Sumsub integration with Wordpress
Version: 1.0.1
Requires at least: 4.5
Tested up to: 5.3.2
Author: oss-kyc
Text Domain: wp-sum-sub
Domain Path: /languages/
 */
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly
if (!defined('WSS_PLUGIN_DIR'))
    define( 'WSS_PLUGIN_DIR', dirname(__FILE__) );
if (!defined('WSS_PLUGIN_ROOT_PHP'))
    define( 'WSS_PLUGIN_ROOT_PHP', dirname(__FILE__).'/'.basename(__FILE__)  );
if(!defined('WSS_PLUGIN_ABSOLUTE_PATH'))
    define('WSS_PLUGIN_ABSOLUTE_PATH',plugin_dir_url(__FILE__));
if (!defined('WSS_PLUGIN_ADMIN_DIR'))
    define( 'WSS_PLUGIN_ADMIN_DIR', dirname(__FILE__) . '/admin' );
if (!defined('WSS_TEXT_DOMAIN'))
    define( 'WSS_TEXT_DOMAIN', 'wp-sum-sub' );

if( !class_exists('WP_Sumsub') ) {
    class WP_Sumsub{
        public function __construct() {
            require_once(WSS_PLUGIN_DIR.'/vendor/autoload.php');
            require_once(WSS_PLUGIN_ADMIN_DIR.'/class-admin.php');
            require_once(WSS_PLUGIN_DIR.'/includes/class-access-token.php');
            require_once(WSS_PLUGIN_DIR.'/includes/class-scripts-styles.php');
            require_once(WSS_PLUGIN_DIR.'/includes/class-front-end.php');
            add_filter('plugin_action_links_'.plugin_basename(__FILE__), [&$this,'add_setting_link']);
        }
        public function add_setting_link($links){
            array_unshift($links, '<a href="' .
            admin_url( 'admin.php?page=wss-options' ) .
            '">' . __('Settings',WSS_TEXT_DOMAIN) . '</a>');
        return $links;
        }
        public static function getAccessToken($externalUserId,$levelName){
            $token=get_option('wss_setting_token','');
            $secret=get_option('wss_setting_secret','');
            $url=get_option('wss_setting_url','https://api.sumsub.com');
          
            $classObject=new WSS_Access_Token($token,$secret,$url);
            $applicantId = $classObject->createApplicant($externalUserId, $levelName);
            if(!empty($applicantId)){
                $accessTokenStr = $classObject->getAccessToken($externalUserId, $levelName);
                if(!empty($accessTokenStr)){
                    return json_decode($accessTokenStr);
                }
            }
            return [];
        }
    }
    new WP_Sumsub();
}